-- Setup an initial state object and provide functions for manipulating that state.
primaryFARPS = {"FARP Alpha", "FARP Bravo", "FARP Charlie", "FARP Delta"}
primaryAirfields = {"Bandar Abbas Intl"}
objective_spawn_zones = {
    ["spawn_1_1"] = Airbase.getByName("Al Maktoum Intl"),
    ["spawn_1_2"] = Airbase.getByName("Al Ain International Airport"),
    ["spawn_1_3"] = Airbase.getByName("FARP Bravo"),
    ["spawn_2_1"] = Airbase.getByName("Dubai Intl"),
    ["spawn_2_2"] = Airbase.getByName("Fujairah Intl"),
    ["spawn_2_3"] = Airbase.getByName("Sharjah Intl"),
    ["spawn_3_1"] = Airbase.getByName("Khasab"),
    ["spawn_3_2"] = Airbase.getByName("Khasab"),
    ["spawn_3_3"] = Airbase.getByName("Khasab"),
}

game_state = {
    ["last_launched_time"] = 0,
    ["last_cap_spawn"] = 0,
    ["Phase Airfields"] = {},
    ["Airfields"] = {},
    ["Airfield Defenders"] = {},
    ["Primary"] = {
        ["Bandar Abbas Intl"] = false,
    },
    ["StrategicSAM"] = {},
    ["C2"] = {},
    ["EWR"] = {},
    ["CASTargets"] = {},
    ["StrikeTargets"] = {},
    ["TheaterObjectives"] = {},
    ["InterceptTargets"] = {},
    ["DestroyedStatics"] = {},
    ["OpforCAS"] = {},
    ["CAP"] = {},
    ["BAI"] = {},
    ["AWACS"] = {},
    ["Tanker"] = {},
    ["NavalStrike"] = {},
    ["CTLD_ASSETS"] = {},
    ['Convoys'] ={},
    ["last_redfor_cap"] = 0,
    ["FARPS"] = {}
}


local airfields
for i=0,2 do
    airfields = coalition.getAirbases(i)
    for idx,ab in ipairs(airfields) do
        local name = ab:getName()
        if string.match(name, 'FARP') then
            game_state["FARPS"][ab:getName()] = ab:getCoalition()
        else
            game_state["Airfields"][ab:getName()] = ab:getCoalition()
        end
    end
end

local aircraft
abslots = {}
logislots = {}
for ci = 0,2 do
    airfields = coalition.getAirbases(ci)
    aircraft = coalition.getGroups(2)
    for i,ab in ipairs(airfields) do
        logislots[ab:getName()] = nil
        abslots[ab:getName()] = {}
        for i,ac in ipairs(aircraft) do
            if string.match(ac:getName(), ab:getName()) then
                table.insert(abslots[ab:getName()], ac:getName())
            end
        end
    end
end

game_stats = {
    c2    = {
        alive = 0,
        nominal = 3,
        tbl   = game_state["C2"],
    },
    ewr = {
        alive = 0,
        nominal = 2,
        tbl   = game_state["EWR"],
    },
    awacs = {
        alive = 0,
        nominal = 1,
        tbl   = game_state["AWACS"],
    },
    bai = {
        alive = 0,
        nominal = 3,
        tbl = game_state["BAI"],
    },
    ammo = {
        alive = 0,
        nominal = 2,
        tbl   = game_state["StrikeTargets"],
        subtype = "AmmoDump",
    },
    comms = {
        alive = 0,
        nominal = 2,
        tbl   = game_state["StrikeTargets"],
        subtype = "CommsArray",
    },
    caps = {
        alive = 0,
        nominal = 5,
        tbl = game_state["CAP"],
    },
    airports = {
        alive = 0,
        nominal = 3,
        tbl = game_state["Airfields"],
    },
}

game_state["Phase Airfields"] =  {
    {Airbase.getByName("Al Ain International Airport"), Airbase.getByName("Al Maktoum Intl"), Airbase.getByName("FARP ALPHA"), Airbase.getByName("FARP BRAVO")},
    {Airbase.getByName("Al Minhad AB"), Airbase.getByName("Dubai Intl"), Airbase.getByName("Fujairah Intl"), Airbase.getByName("Sharjah Intl")},
    {Airbase.getByName("FARP CHARLIE"), Airbase.getByName("FARP DELTA"), Airbase.getByName("Khasab")}
}

logiSlots = {}

late_unlockables = {
    "Al Minhad AB Ka-50 - 1",
    "Al Minhad AB Ka-50 - 2",
    "Al Minhad AB Ka-50 - 3",
    "Al Minhad AB Ka-50 - 2",
    "Al Minhad AB Gazelle L - 1",
    "Al Minhad AB Gazelle L - 2",
    "Al Minhad AB Gazelle M - 1",
    "Al Minhad AB Gazelle M - 2",
    "Al Minhad AB UH-1H - 1",
    "Al Minhad AB UH-1H - 2",
    "Al Minhad AB UH-1H - 3",
    "Al Minhad AB UH-1H - 4",
    "Al Minhad AB Mi-8 - 1",
    "Al Minhad AB Mi-8 - 2",
    "Al Minhad AB Mi-8 - 3",
    "Al Minhad AB Mi-8 - 4"
}

log("Game State INIT")
