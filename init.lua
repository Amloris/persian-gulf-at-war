local statefile = io.open(lfs.writedir() .. "Scripts\\PGAW\\state.json", 'r')

-- Enable slotblock
trigger.action.setUserFlag("SSB",100)
if statefile then
    trigger.action.outText("Found a statefile.  Processing it instead of starting a new game", 40)
    local state = statefile:read("*all")
    statefile:close()
    local saved_game_state = json:decode(state)
    trigger.action.outText("Game state read", 10)

    -- Spawn any defenders not killed last round
    for template_name, group_name in pairs(saved_game_state["Airfield Defenders"]) do
        local spawner = Spawner(template_name)
        local new_grp = spawner:Spawn()
        game_state["Airfield Defenders"][template_name] = new_grp:getName()
        log("Spawned surviving AP DEF group " .. template_name)
    end

    -- Spawn a defense force if blue owns an airfield.  Set the slotblock flag for the airfield if it needs it
    for name, coalition in pairs(saved_game_state["Airfields"]) do
        if not string.match(name, "Unit") and not string.match(name, "Al Dhafra AB") then
            local flagval = 100
            local ab = Airbase.getByName(name)
            local apV3 = ab:getPosition().p
            local posx = 0
            local posy = 0
            if string.match("Sas Al Nakheel Airport", name) then
                posx = apV3.x
                posy = apV3.z
            else
                posx = apV3.x + math.random(900, 1100)
                posy = apV3.z - math.random(150, 270)
            end
            game_state["Airfields"][name] = coalition

            if coalition == 1 then
                -- just set the AB spawns flag if we need it.  The defense forces left already spawned above.
                if AirbaseSpawns[name] then flagval = 100 end
            elseif coalition == 2 then
                if not string.match(name, "Al Dhafra AB") then
                    -- Spawn a defense force, a support force, and active a logi slot if one exists for this AB
                    AirfieldDefense:SpawnAtPoint({
                        x = posx,
                        y = posy
                    })

                posx = posx + math.random(100, 200)
                posy = posy + math.random(100, 200)
                BlueFarpSupportGroups[name] = FSW:SpawnAtPoint({x=posx, y=posy})
                flagval = 0

                --if ab_logi_slots[name] then
                --    activateLogi(ab_logi_slots[name])
                --end
                end
            end

            if abslots[name] then
                for i,grp in ipairs(abslots[name]) do
                    trigger.action.setUserFlag(grp, flagval)
                end
            end
        end
    end

    trigger.action.outText("Finished processing airfields", 10)

    for name, coalition in pairs(saved_game_state["FARPS"]) do
        if not string.match(name, "Abu Dhabi Area FARP") then
            local flagval = 100
            local ab = Airbase.getByName(name)
            local apV3 = ab:getPosition().p

            apV3.x = apV3.x + math.random(-25, 25)
            apV3.z = apV3.z + math.random(-25, 25)
            game_state["FARPS"][name] = coalition

            if coalition == 1 then
                flagval = 100
            elseif coalition == 2 then
                BlueSecurityForcesGroups[name] = AirfieldDefense:SpawnAtPoint(apV3)
                apV3.x = apV3.x + 50
                apV3.z = apV3.z - 50
                BlueFarpSupportGroups[name] = FSW:SpawnAtPoint({x=apV3.x, y=apV3.z}, true)
                flagval = 0

                --if ab_logi_slots[name] then
                --    activateLogi(ab_logi_slots[name])
                --end
            end

            if abslots[name] then
                for i,grp in ipairs(abslots[name]) do
                    trigger.action.setUserFlag(grp, flagval)
                end
            end
        end
    end

    trigger.action.outText("Finished processing FARPs", 10)

    for name, data in pairs(saved_game_state["StrategicSAM"]) do
        local spawn
        if data.spawn_name == "SA6" then spawn = RussianTheaterSA6Spawn[1] end
        if data.spawn_name == "SA10" then spawn = RussianTheaterSA10Spawn[1] end
        spawn:SpawnAtPoint({
            x = data['last_position'].x,
            y = data['last_position'].z
        })
    end

    for name, data in pairs(saved_game_state["C2"]) do
        RussianTheaterC2Spawn[1]:SpawnAtPoint({
            x = data['last_position'].x,
            y = data['last_position'].z
        })
    end

    for name, data in pairs(saved_game_state["EWR"]) do
        RussianTheaterEWRSpawn[1]:SpawnAtPoint({
            x = data['last_position'].x,
            y = data['last_position'].z
        })
    end

    trigger.action.outText("Finished processing strategic assets", 10)

    for name, data in pairs(saved_game_state["StrikeTargets"]) do
        local spawn
        log('spawning ' .. data['spawn_name'])
        if data['spawn_name'] == 'AmmoDump' then spawn = AmmoDumpSpawn end
        if data['spawn_name'] == 'CommsArray' then spawn = CommsArraySpawn end
        if data['spawn_name'] == 'PowerPlant' then spawn = PowerPlantSpawn end
        local static = spawn:Spawn({
            data['position'].x,
            data['position'].z
        })
    end


    for name, data in pairs(saved_game_state["BAI"]) do
        local spawn
        if data['spawn_name'] == "ASSAULT" then spawn = Spawner("assault_1_1") end
        if data['spawn_name'] == "ARTILLERY" then spawn = RussianHeavyArtySpawn[1] end
        if data['spawn_name'] == "ARMOR COLUMN" then spawn = ArmorColumnSpawn[1] end
        if data['spawn_name'] == "MECH INF" then spawn = MechInfSpawn[1] end
        local baitarget = spawn:SpawnAtPoint({
            x = data['last_position'].x,
            y = data['last_position'].z
        })
        mist.scheduleFunction(function()
            local tgt_zone = data['objective']
            mist.groupToPoint(baitarget:getName(), tgt_zone, 'Custom', nil, 50, true)
            game_state["BAI"][baitarget:getName()]["objective"] = tgt_zone
        end, {}, timer.getTime()+ 10)

    end

    trigger.action.outText("Finished processing BAI", 10)

    local theaterobjs = saved_game_state["TheaterObjectives"]
    if theaterobjs ~= nil then
      for name, data in pairs(saved_game_state["TheaterObjectives"]) do
        local spawner = TheaterObjectives[name]
        if not spawner then
          log("Found TheaterObjective " .. name .. " but no spawner for it!")
        else
          log(" Spawning TheaterObjective " .. name)
          spawner:Spawn()
        end
      end
    end

    for idx, data in ipairs(saved_game_state["CTLD_ASSETS"]) do
        if data.name == 'avenger' then
            avengerspawn:SpawnAtPoint({
                x = data.pos.x,
                y = data.pos.z
            })
        end

        if data.name == 'ammo' then
            ammospawn:SpawnAtPoint({
                x = data.pos.x,
                y = data.pos.z
            })
        end

        if data.name == 'gepard' then
            gepardspawn:SpawnAtPoint({
                x = data.pos.x,
                y = data.pos.z
            })
        end

        if data.name == 'mlrs' then
            mlrsspawn:SpawnAtPoint({
                x = data.pos.x,
                y = data.pos.z
            })
        end

        if data.name == 'jtac' then
            local _spawnedGroup = jtacspawn:SpawnAtPoint({
                x = data.pos.x,
                y = data.pos.z
            })
            local _code = table.remove(ctld.jtacGeneratedLaserCodes, 1)
            table.insert(ctld.jtacGeneratedLaserCodes, _code)
            ctld.JTACAutoLase(_spawnedGroup:getName(), _code)
        end
    end

    local destroyedStatics = saved_game_state["DestroyedStatics"]
    if destroyedStatics ~= nil then
        for k, v in pairs(destroyedStatics) do
            local obj = StaticObject.getByName(k)
            if obj ~= nil then
                StaticObject.destroy(obj)
            end
        end
        game_state["DestroyedStatics"] = saved_game_state["DestroyedStatics"]
    end

    local CTLDstate = saved_game_state["Hawks"]
    if CTLDstate ~= nil then
        for k,v in pairs(CTLDstate) do
            respawnHAWKFromState(v)
        end
    end

    game_state["CTLD_ASSETS"] = saved_game_state["CTLD_ASSETS"]

else
    -- Populate the world and gameplay environment.
    trigger.action.outText("No state file detected.  Creating new situation", 10)
    for i=1, 6 do
        local zone_index = math.random(20)
        local zone = "NorthSA6Zone"
        RussianTheaterSA6Spawn[1]:SpawnInZone(zone .. zone_index)
    end

    for i=1, 7 do
        if i < 4 then
            local zone_index = math.random(8)
            local zone = "NorthSA10Zone"
            RussianTheaterSA10Spawn[1]:SpawnInZone(zone .. zone_index)
        end

        local zone_index = math.random(8)
        local zone = "NorthSA10Zone"
        RussianTheaterEWRSpawn[1]:SpawnInZone(zone .. zone_index)

        local zone_index = math.random(8)
        local zone = "NorthSA10Zone"
        RussianTheaterC2Spawn[1]:SpawnInZone(zone .. zone_index)
    end

    for i=1, 10 do
        local zone_index = math.random(18)
        local zone = "NorthStatic" .. zone_index
        local StaticSpawns = {AmmoDumpSpawn, PowerPlantSpawn, CommsArraySpawn}
        local spawn_index = math.random(3)
        local vec2 = mist.getRandomPointInZone(zone)
        local id = StaticSpawns[spawn_index]:Spawn({vec2.x, vec2.y})
    end

    for gidx, group in ipairs(coalition.getGroups(1, 2)) do
        local groupname = Group.getName(group)
        if string.match(groupname, "AP DEF") then
            local spawned_group = Spawner(groupname):Spawn()
            game_state["Airfield Defenders"][groupname] = spawned_group:getName()
        end
    end    

    -- Disable slots
    for idx=0,2 do
        for abid, ab in pairs(coalition.getAirbases(idx)) do
            log(ab:getName() .. " - " .. ab:getID())
            for i,ac in ipairs(late_unlockables) do
                if string.match(ac, ab:getName()) then
                    trigger.action.setUserFlag(ac,100)
                    log("Setting flag: " .. ac .. " to 100")
                end
            end
        end
    end
end

-- Kick off supports
mist.scheduleFunction(function()
    -- Friendly
    TexacoSpawn:Spawn()
    ShellSpawn:Spawn()
    OverlordSpawn:Spawn()
    ArcoSpawn:Spawn()
    -- Enemy
    RussianTheaterAWACSSpawn:Spawn()
end, {}, timer.getTime() + 10)

mist.scheduleFunction(function()
  RussianTheaterCASSpawn:Spawn()
  RussianTheaterCASSpawnF14:Spawn()
  log("Spawned CAS Groups...")
end, {}, timer.getTime() + 10, 1800)
-- Kick off the commanders
mist.scheduleFunction(russian_commander, {}, timer.getTime() + 120, 1000)
log("init.lua complete")
